package com.zuitt.discussion.config;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serializable;

// When a user tries to access a protected resource without proper authentication, an "AuthenticationException" is thrown and the "commence()" method is invoked to handle this exception
public class JwtAuthenticate implements AuthenticationEntryPoint, Serializable {

    // serialVersionUID serves as the "state" of a serializable object. This is used by Java in deserializing a

    private static final long serialVersionUID = -7858869558953243875L;

    @Override
    // This will handle the authentication failures.
    public void commence(HttpServletRequest request, HttpServletResponse response,

                         AuthenticationException authException) throws IOException {

        response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized");

    }
}
